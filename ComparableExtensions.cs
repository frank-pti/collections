using System;

namespace FrankPti.Collections
{
    /// <summary>
    /// Extension methods for the coomparable interface. 
    /// </summary>
	public static class ComparableExtensions
	{
        /// <summary>
        /// Test whether a comparable object is greater than another.
        /// </summary>
        /// <returns><c>true</c>, if <c>a</c> compared to <c>b</c> is greater than 0, <c>false</c> otherwise.</returns>
        /// <param name="a">The first value.</param>
        /// <param name="b">The second value.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
		public static bool GreaterThan(this IComparable a, object b)
		{
			return a.CompareTo(b) > 0;
		}

        /// <summary>
        /// Test whether a copmarable object is greater than or equal to another.
        /// </summary>
        /// <returns><c>true</c>, if <c>a</c> compared to <c>b</c> is greater than or equal to 0, <c>false</c> otherwise.</returns>
        /// <param name="a">The alpha component.</param>
        /// <param name="b">The blue component.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
		public static bool GreaterOrEqualThan(this IComparable a, object b)
		{
			return a.CompareTo(b) >= 0;
		}

        /// <summary>
        /// Tests whether a comparable object is lower than another.
        /// </summary>
        /// <returns><c>true</c>, if <c>a</c> compared to <c>b</c> is lower than 0, <c>false</c> otherwise.</returns>
        /// <param name="a">The alpha component.</param>
        /// <param name="b">The blue component.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
		public static bool LowerThan(this IComparable a, object b)
		{
			return a.CompareTo(b) < 0;
		}

        /// <summary>
        /// Test whether a copmarable object is lower than or equal to another.
        /// </summary>
        /// <returns><c>true</c>, if <c>a</c> compared to <c>b</c> is lower than or equal to 0, <c>false</c> otherwise.</returns>
        /// <param name="a">The alpha component.</param>
        /// <param name="b">The blue component.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
		public static bool LowerOrEqualThan(this IComparable a, object b)
		{
			return a.CompareTo(b) <= 0;
		}
	}
}

