﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FrankPti.Collections
{
    public class MultipleException : Exception
    {
        public MultipleException(string message, IList<Exception> exceptions) :
            base(message)
        {
            Exceptions = exceptions;
        }

        public IList<Exception> Exceptions
        {
            get;
            private set;
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(base.ToString());
            builder.AppendLine("Containing exceptions:");
            if (Exceptions.Count > 0) {
                foreach (Exception e in Exceptions) {
                    builder.AppendLine(e.ToString());
                }
            }
            builder.AppendLine("---");
            return builder.ToString();
        }
    }
}
