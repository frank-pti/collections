/*
 * A DotNet collections library
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;

namespace FrankPti.Collections
{
    /// <summary>
    /// Dictonary where the keys are the fields of an enumeration type.
    /// </summary>
    public class EnumDictionary<TEnum, T>:
        IDictionary<TEnum, T>
            where TEnum: struct
    {
        private Dictionary<TEnum, T> dictionary;

        /// <summary>
        /// Initializes a new instance of the <see cref="FrankPti.Collections.EnumDictionary`2"/> class.
        /// </summary>
        public EnumDictionary()
        {
            dictionary = new Dictionary<TEnum, T>();
            foreach(object oEnum in Enum.GetValues(typeof(TEnum))) {
                TEnum e = (TEnum)oEnum;
                dictionary.Add(e, default(T));
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="FrankPti.Collections.EnumDictionary`2"/> class.
        /// </summary>
        /// <param name="other">Other.</param>
        public EnumDictionary(IDictionary<TEnum, T> other)
        {
            if (other == null) {
                other = new Dictionary<TEnum, T> ();
            }
            dictionary = new Dictionary<TEnum, T>();
            foreach(object oEnum in Enum.GetValues(typeof(TEnum))) {
                TEnum e = (TEnum)oEnum;
                if (other.ContainsKey(e)) {
                    dictionary.Add (e, other[e]);
                } else {
                    dictionary.Add(e, default(T));
                }
            }
        }

        #region IDictionary implementation
        /// <summary>
        /// Add the specified key and value. If the key already exists in the dictionary it is replaced.
        /// </summary>
        /// <param name="key">Key.</param>
        /// <param name="value">Value.</param>
        public void Add(TEnum key, T value)
        {
            if(dictionary.ContainsKey(key)) {
                dictionary[key] = value;
            } else {
                dictionary.Add(key, value);
            }
        }

        /// <Docs>The key to locate in the current instance.</Docs>
        /// <para>Determines whether the current instance contains an entry with the specified key.</para>
        /// <summary>
        /// Containses the key.
        /// </summary>
        /// <returns><c>true</c>, if key was containsed, <c>false</c> otherwise.</returns>
        /// <param name="key">Key.</param>
        public bool ContainsKey(TEnum key)
        {
            return true;
        }

        /// <Docs>The item to remove from the current collection.</Docs>
        /// <para>Removes the first occurrence of an item from the current collection.</para>
        /// <summary>
        /// Remove the specified key.
        /// </summary>
        /// <param name="key">Key.</param>
        public bool Remove(TEnum key)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Try getting the value.
        /// </summary>
        /// <remarks>This method combines the functionality of the <see cref="ContainsKey"/> method and the 
        /// <see cref="Item"/> property</remarks>.
        /// <returns><c>true</c>, if get value was tryed, <c>false</c> otherwise.</returns>
        /// <param name="key">The Key of the value to get.</param>
        /// <param name="value">When this method returns, contains the valule associated with the specified key.</param>
        public bool TryGetValue(TEnum key, out T value)
        {
            return dictionary.TryGetValue(key, out value);
        }

        /// <summary>
        /// Gets or sets the <see cref="FrankPti.Collections.EnumDictionary`2"/> with the specified key.
        /// </summary>
        /// <param name="key">Key.</param>
        public T this[TEnum key] {
            get {
                return dictionary[key];
            }
            set {
                dictionary[key] = value;
            }
        }

        /// <summary>
        /// Gets the keys.
        /// </summary>
        /// <value>The keys.</value>
        public ICollection<TEnum> Keys {
            get {
                return dictionary.Keys;
            }
        }

        /// <summary>
        /// Gets the values.
        /// </summary>
        /// <value>The values.</value>
        public ICollection<T> Values {
            get {
                return dictionary.Values;
            }
        }
        #endregion

        private ICollection<KeyValuePair<TEnum, T>> DictionaryCollection {
            get {
                return (ICollection<KeyValuePair<TEnum, T>>)dictionary;
            }
        }

        #region ICollection implementation
        /// <remarks>Adds an item to the collection.</remarks>
        /// <exception cref="System.NotSupportedException">The current collection is read-only.</exception>
        /// <summary>
        /// Add the specified item.
        /// </summary>
        /// <param name="item">Item.</param>
        public void Add(KeyValuePair<TEnum, T> item)
        {
            Add(item.Key, item.Value);
        }

        /// <summary>
        /// Clear this instance.
        /// </summary>
        public void Clear()
        {
            throw new NotSupportedException();
        }

        /// <Docs>The object to locate in the current collection.</Docs>
        /// <para>Determines whether the current collection contains a specific value.</para>
        /// <summary>
        /// Contains the specified item.
        /// </summary>
        /// <param name="item">Item.</param>
        public bool Contains(KeyValuePair<TEnum, T> item)
        {
            return DictionaryCollection.Contains(item);
        }

        /// <summary>
        /// Copy elements of the collection to the specified array starting at the specified index.
        /// </summary>
        /// <param name="array">Array.</param>
        /// <param name="arrayIndex">Array index.</param>
        public void CopyTo(KeyValuePair<TEnum, T>[] array, int arrayIndex)
        {
            DictionaryCollection.CopyTo(array, arrayIndex);
        }

        /// <Docs>The item to remove from the current collection.</Docs>
        /// <para>Removes the first occurrence of an item from the current collection.</para>
        /// <summary>
        /// Remove the specified item.
        /// </summary>
        /// <param name="item">Item.</param>
        public bool Remove(KeyValuePair<TEnum, T> item)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Gets the count.
        /// </summary>
        /// <value>The count.</value>
        public int Count {
            get {
                return dictionary.Count;
            }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is read only.
        /// </summary>
        /// <value><c>true</c> if this instance is read only; otherwise, <c>false</c>.</value>
        public bool IsReadOnly {
            get {
                return false;
            }
        }
        #endregion

        #region IEnumerable implementation
        /// <summary>
        /// Gets the enumerator.
        /// </summary>
        /// <returns>The enumerator.</returns>
        public IEnumerator<KeyValuePair<TEnum, T>> GetEnumerator()
        {
            return dictionary.GetEnumerator();
        }
        #endregion

        #region IEnumerable implementation
        /// <summary>
        /// Gets the enumerator.
        /// </summary>
        /// <returns>The enumerator.</returns>
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return dictionary.GetEnumerator();
        }
        #endregion
    }
}

