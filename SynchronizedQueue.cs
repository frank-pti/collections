/*
 * A DotNet collections library
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System.Collections.Generic;
using System.Threading;

namespace FrankPti.Collections
{
    /// <summary>
    /// A generic synchronized queue.
    /// </summary>
    /// <typeparam name="T">
    /// The type of the elements that are placed in the queue.
    /// </typeparam>
    public class SynchronizedQueue<T>
    {
        private Queue<T> queue;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public SynchronizedQueue()
        {
            queue = new Queue<T>();
        }

        /// <summary>
        /// Enqueue an element into the queue.
        /// </summary>
        /// <param name="element">
        /// The element which is enqueued.
        /// </param>
        public void Enqueue(T element)
        {
            lock (queue) {
                queue.Enqueue(element);
                Monitor.PulseAll(queue);
            }
        }

        /// <summary>
        /// Dequeue an element from the queue.
        /// </summary>
        /// <remarks>
        /// If the queue is empty, this method blocks until an element was enqueued.
        /// </remarks>
        /// <returns>
        /// The dequeued element.
        /// </returns>
        public T Dequeue()
        {
            T result;
            lock (queue) {
                if (queue.Count == 0) {
                    Monitor.Wait(queue);
                }
                result = queue.Dequeue();
            }
            return result;
        }

        /// <summary>
        /// Dequeue an element from the queue with timeout.
        /// </summary>
        /// <param name='millisecondsTimeout'>
        /// Milliseconds timeout.
        /// </param>
        /// <returns>
        /// The dequeued element, or default(T) if the operation timed out.
        /// </returns>
        public T Dequeue(int millisecondsTimeout)
        {
            T result = default(T);
            lock (queue) {
                bool success = true;
                if (queue.Count == 0) {
                    success = Monitor.Wait(queue, millisecondsTimeout);
                }
                if (success) {
                    result = queue.Dequeue();
                }
            }
            return result;
        }

        /// <summary>
        /// The number of elements inside the queue.
        /// </summary>
        public int Count
        {
            get
            {
                lock (queue) {
                    return queue.Count;
                }
            }
        }

        /// <summary>
        /// Tests if the queue contains the specified item
        /// </summary>
        /// <param name="item">the item to be found</param>
        /// <returns><c>true</c> if the queue contains the item, otherwise <c>false</c>.</returns>
        public bool Contains(T item)
        {
            lock (queue) {
                return queue.Contains(item);
            }
        }
    }
}