/*
 * A DotNet collections library
 * Copyright (C) Wolfgang Silbermayr
 * Copyright (C) Florian Marchl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Collections;

namespace FrankPti.Collections
{
	/// <summary>
	/// A dictionary to which no contents can be added or removed.
	/// </summary>
	/// <remarks>
	/// Neither the keys nor the values of this dictionary type can be reassigned.
	/// Therefore there is no constructor with an empty parameter list, but the
	/// simplest constructor must at least contain an IDictionary from which
	/// the keys and values are copied.
	/// </remarks>
    public class ImmutableDictionary<TKey,TValue> :
        IDictionary<TKey,TValue>,
        ICollection<KeyValuePair<TKey,TValue>>,
        IEnumerable<KeyValuePair<TKey,TValue>>,
        IEnumerable
    {
        IDictionary<TKey,TValue> dictionary;

		/// <summary>
		/// The default constructor.
		/// </summary>
		/// <param name="dictionary">
		/// The <see cref="T:IDictionary{TKey, TValue}"/> from which the values are copied.
		/// </param>
        public ImmutableDictionary (IDictionary<TKey,TValue> dictionary)
        {
            if (dictionary == null) {
                throw new ArgumentNullException ("dictionary");
            }
            this.dictionary = dictionary;
        }

        #region IDictionary[TKey,TValue] implementation
        void IDictionary<TKey, TValue>.Add (TKey key, TValue value)
        {
            throw new NotImplementedException ("Immutable dictionary is readonly");
        }

        bool IDictionary<TKey, TValue>.ContainsKey (TKey key)
        {
            return dictionary.ContainsKey(key);
        }

        bool IDictionary<TKey, TValue>.Remove (TKey key)
        {
            throw new NotImplementedException ("Cannot remove key from an immutable dictionary");
        }

        bool IDictionary<TKey, TValue>.TryGetValue (TKey key, out TValue value)
        {
            return dictionary.TryGetValue(key, out value);
        }

		/// <summary>
		/// Get the value at a specified index.
		/// </summary>
		/// <param name="key">
		/// The index.
		/// </param>
        public TValue this[TKey key] {
            get {
                return dictionary[key];
            }
            set {
                throw new NotImplementedException ("Cannot set value of an immutable dictionary");
            }
        }

        ICollection<TKey> IDictionary<TKey, TValue>.Keys
        {
            get {
                return dictionary.Keys;
            }
        }

        ICollection<TValue> IDictionary<TKey, TValue>.Values {
            get {
                return dictionary.Values;
            }
        }
        #endregion

        #region ICollection[KeyValuePair[TKey,TValue]] implementation
        /// <summary>
        /// Add is not supported on the immutable dictionary.
        /// </summary>
        /// <param name="item">Unused</param>
        /// <exception cref="System.NotSupportedException">
        /// Cannot add anything to an immutable dictionary.
        /// </exception>
        public void Add (KeyValuePair<TKey, TValue> item)
        {
            throw new NotSupportedException ("Cannot add items to immutable dictionary");
        }

        /// <summary>
        /// Clear is not supported on the immutable dictionary.
        /// </summary>
        /// <exception cref="System.NotSupportedException">
        /// An immutable dictionary cannot be cleared.
        /// </exception>
        public void Clear ()
        {
            throw new NotSupportedException ("Cannot clear an immutable dictionary");
        }

        /// <summary>
        /// Check if a key-value pair is contained in the dictionary.
        /// </summary>
        /// <param name="item">
        /// The <see cref="M:KeyValuePair{TKey, TValue}"/> for which to check.
        /// </param>
        /// <returns>
        /// True if the pair is contained, otherwise false.
        /// </returns>
        public bool Contains (KeyValuePair<TKey, TValue> item)
        {
            return dictionary.Contains(item);
        }

        /// <summary>
        /// Copy the elements of the dictionary to an <see cref="System.Array"/>, starting at a particular <see cref="System.Array"/> index.
        /// </summary>
        /// <param name="array">
        /// The array to which the elements are copied.
        /// </param>
        /// <param name="arrayIndex">
        /// The zero-based index in array at which copying begins.
        /// </param>
        public void CopyTo (KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            dictionary.CopyTo(array, arrayIndex);
        }

        /// <summary>
        /// The number of elements in the dictionary.
        /// </summary>
        public int Count {
            get {
                return dictionary.Count;
            }
        }

        /// <summary>
        /// The immutable dictionary is readonly.
        /// </summary>
        public bool IsReadOnly {
            get {
                return true;
            }
        }

        /// <summary>
        /// Remove is not supported on the immutable dictionary.
        /// </summary>
        /// <param name="item">Unused.</param>
        /// <returns>Nothing.</returns>
        /// <exception cref="System.NotSupportedException">
        /// Cannot remove anything from an immutable dictionary.
        /// </exception>
        public bool Remove (KeyValuePair<TKey, TValue> item)
        {
            throw new NotSupportedException ("Cannot remove item from an immutable dictionary");
        }
        #endregion

        #region IEnumerable implementation
        IEnumerator IEnumerable.GetEnumerator ()
        {
            return dictionary.GetEnumerator();
        }
        #endregion

        #region IEnumerable[KeyValuePair[TKey,TValue]] implementation
        IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator ()
        {
            return dictionary.GetEnumerator();
        }
        #endregion
    }
}

