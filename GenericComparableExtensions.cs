using System;

namespace FrankPti.Collections
{
    /// <summary>
    /// Extension methods for generic comparable interface.
    /// </summary>
	public static class GenericComparableExtensions
	{
        /// <summary>
        /// Test whether a comparable object is greater than another.
        /// </summary>
        /// <returns><c>true</c>, if <c>a</c> compared to <c>b</c> is greater than 0, <c>false</c> otherwise.</returns>
        /// <param name="a">The first value.</param>
        /// <param name="b">The second value.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
		public static bool GreaterThan<T>(this IComparable<T> a, T b)
		{
			return a.CompareTo(b) > 0;
		}

        /// <summary>
        /// Test whether a copmarable object is greater than or equal to another.
        /// </summary>
        /// <returns><c>true</c>, if <c>a</c> compared to <c>b</c> is greater than or equal to 0, <c>false</c> otherwise.</returns>
        /// <param name="a">The alpha component.</param>
        /// <param name="b">The blue component.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
		public static bool GreaterOrEqualThan<T>(this IComparable<T> a, T b)
		{
			return a.CompareTo(b) >= 0;
		}

        /// <summary>
        /// Tests whether a comparable object is lower than another.
        /// </summary>
        /// <returns><c>true</c>, if <c>a</c> compared to <c>b</c> is lower than 0, <c>false</c> otherwise.</returns>
        /// <param name="a">The alpha component.</param>
        /// <param name="b">The blue component.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
		public static bool LowerThan<T>(this IComparable<T> a, T b)
		{
			return a.CompareTo(b) < 0;
		}

        /// <summary>
        /// Test whether a copmarable object is lower than or equal to another.
        /// </summary>
        /// <returns><c>true</c>, if <c>a</c> compared to <c>b</c> is lower than or equal to 0, <c>false</c> otherwise.</returns>
        /// <param name="a">The alpha component.</param>
        /// <param name="b">The blue component.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
		public static bool LowerOrEqualThan<T>(this IComparable<T> a, T b)
		{
			return a.CompareTo(b) <= 0;
		}
	}
}

